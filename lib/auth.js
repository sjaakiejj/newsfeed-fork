function Auth(){
	this.sessions = {};
}

Auth.prototype.login = function( username, password, callback ){
	var self = this;
	var User = require("./user");
	var Session = require("./session");

	// Check if the user exists
	db.users.findOne({username: username, password:password}, function(err,res){
		if(err || !res){
			callback("User not authenticated");
		} else {
			var user = new User(res);
			var session = new Session(user);

			self.sessions[session._id] = session;

			callback(err,session);
		}
	});
}

Auth.prototype.check = function( session ){
	return this.sessions[session._id] &&
				this.sessions[session._id].user == session.user;
}

Auth.prototype.logout = function( session, callback ){
	if( this.sessions[ session._id ] )
		delete this.sessions[ session._id ];
}

module.exports = Auth;